﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;

namespace AuioDirection

{
    public partial class Form1 : Form
    {
        public WaveIn Recorder_NAudio;
        public WaveFileWriter Writer_NAudio;
        MeteringSampleProvider meter;
        BufferedWaveProvider bufferedWaveProvider;
        SampleChannel sampleChannel;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Recorder_NAudio = new WaveIn();
            Recorder_NAudio.DataAvailable += new EventHandler<WaveInEventArgs>(Recorder2_DataAvailable);
            bufferedWaveProvider = new BufferedWaveProvider(Recorder_NAudio.WaveFormat);
            bufferedWaveProvider.DiscardOnBufferOverflow = true;
            sampleChannel = new SampleChannel(bufferedWaveProvider);
            meter = new MeteringSampleProvider(sampleChannel);
            meter.StreamVolume += new EventHandler<StreamVolumeEventArgs>((s2, e2) =>
            {
                Console.WriteLine(e2.MaxSampleValues[1]);
            });
            Recorder_NAudio.StartRecording();
        }

        private void Recorder2_DataAvailable(object sender, WaveInEventArgs e)
        {
            var tmpBuffer = new float[e.BytesRecorded];
            if (meter != null)
                meter.Read(tmpBuffer, 0, e.BytesRecorded);
        }
    }

}
